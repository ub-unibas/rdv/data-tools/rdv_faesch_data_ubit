import re

from rdv_data_helpers_ubit import IIIF_MANIF_ESFIELD
from rdv_data_helpers_ubit.projects.hierarchy.hierarchy import HIERARCHY_FIELD
from rdv_hierarchy_ubit import MarcDefHierachy
from rdv_entity_data_ubit import RDVEntityData
from rdv_oaimarc_data_ubit import RDVOAIMarcData

class RDVFaeschEntity(RDVEntityData):

    @property
    def link_field(self):
        """field which is used also in Object-Index to create Links"""
        return "adress_empf"

    @property
    def entity_type(self):
        return "AutorIn"

class RDVFasch(RDVOAIMarcData):
    index = "faesch"
    pers_index = "faeschpers"
    pers_index_prefix = "faeschpers"
    entity_class = RDVFaeschEntity

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.marc_hierarchy = MarcDefHierachy(host="localhost:9209",
                                              index="marcdef",
                                              index_prefix="marcdef",
                                              logger=kwargs["logger"])


    def extend_record_data(self, record, bsiz_id):
        record["type"] = "Objekt"
        record[HIERARCHY_FIELD] = []
        for marc_hier_id in record["record_marchier_id"]:

            if self.marc_hierarchy.es_thesaurus.get(marc_hier_id):
                record.setdefault(HIERARCHY_FIELD, []).extend(self.marc_hierarchy.es_thesaurus.get(marc_hier_id,{}).get(HIERARCHY_FIELD))
            else:
                print(marc_hier_id)

        for s in record.get("signatur",[]):
            record.setdefault("split_signatur",[]).append(s)
            s_begin = ""
            for n, s_p in enumerate(s.split(" ")):
                s_begin += " " + s_p if n != 0 else s_p
                if n != 0 and n!= len(s.split(" ")) and s_begin:
                    record["split_signatur"].append(s_begin)

        for e in record.get("581_a",[]):
            e = re.split(", S\. \d{1}", e)
            record.setdefault("581_a_norm", []).append(e[0])

        for e in record.get("510_a",[]):
            e = re.split(", S\. \d{1}", e)
            record.setdefault("510_a_norm", []).append(e[0])

        for e in record.get("300_a",[]):
            e = re.sub("\d{1}", "#", e)
            record.setdefault("300_a_norm", []).append(e)

        for e in record.get("300_c",[]):
            e = re.sub("\d{1}", "#", e)
            record.setdefault("300_c_norm", []).append(e)

        record.setdefault("655_a_7", []).extend(record.get("336_b",[]))

        manif = record.get(IIIF_MANIF_ESFIELD)
        if manif:
            vlm_id = manif[0].split("/")[-2]
            record["vlm_id"] = vlm_id

        for person in record.get("author", []):
            id_ = person.get("id")
            self.entities[id_] = person
            if id_:
                self.object_entity_lookup.setdefault(person.get("id") or person.get("label"), []).append(record)
            record.setdefault("adress_empf",[]).append(person)
        for e in record.get("rel_persons", []):
            role = e.get("role")
            if role and set([role]) & set(["prt", "Drucker"]):
                record.setdefault("Drucker",[]).append(e)


