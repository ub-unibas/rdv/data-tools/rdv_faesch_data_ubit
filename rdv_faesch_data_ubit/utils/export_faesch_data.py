import elasticsearch_dsl7
import elasticsearch

es = elasticsearch.Elasticsearch(hosts="localhost:9209")

search = elasticsearch_dsl7.Search(index="faesch", using=es).query({"match_all": {}})


all_results = {}
for hit in search.scan():
    all_results[hit.meta.id]= hit.to_dict()
    print(hit.meta.id, hit)

import json
json.dump(all_results, open("export_faesch.json", "w"))